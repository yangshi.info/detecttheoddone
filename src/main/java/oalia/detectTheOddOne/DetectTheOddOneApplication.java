package oalia.detectTheOddOne;

import oalia.detectTheOddOne.service.DetectionService;
import oalia.detectTheOddOne.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
public class DetectTheOddOneApplication implements CommandLineRunner {

    @Autowired
    private DetectionService detectionService;

    private static final Logger log = LoggerFactory.getLogger(DetectTheOddOneApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(DetectTheOddOneApplication.class);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (args == null || args.length == 0) {
            log.error("The input argument can't be empty : " + args);
            return;
        }

        String parameterString = args[0];
        if (StringUtils.isBlank(parameterString) || !StringUtils.isNumeric(parameterString)) {
            log.error("The input argument " + parameterString + " isn't allowed");
            return;
        }

        int parameterNumber = Integer.valueOf(parameterString);
        Map<String, Integer> resultToReturn = detectionService.calculateOddDetection(parameterNumber);
        log.info("result for parameter#{} : maximumTry#{}, minimumTry#{}", parameterNumber, resultToReturn.get(Constants.STRING_MAXIMUM_TRY_MAP_KEY), resultToReturn.get(Constants.STRING_MINIMUM_TRY_MAP_KEY));
    }
}
