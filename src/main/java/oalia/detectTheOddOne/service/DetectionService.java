package oalia.detectTheOddOne.service;

import oalia.detectTheOddOne.utils.Constants;
import oalia.detectTheOddOne.utils.MathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DetectionService {

    private static final Logger log = LoggerFactory.getLogger(DetectionService.class);

    public Map<String, Integer> calculateOddDetection(int totalNumberToCheck) throws Exception {
        if(totalNumberToCheck <= 0) {
            throw new Exception("The number totalNumberToCheck#" + totalNumberToCheck + " isn't allowed");
        }

        Map<String, Integer> resultToReturn = new HashMap<>();
        log.info("Processing the odd detection calculation for totalNumberToCheck#{}", totalNumberToCheck);
        double calculatedLog = MathUtils.calculateLog(totalNumberToCheck, Constants.SHORT_LOG_BASE_2);
        log.info("The calculated maximum try for totalNumberToCheck#{} is maximumTry#{}", totalNumberToCheck, (int) calculatedLog);
        resultToReturn.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, (int) calculatedLog);
        int calculatedMinimumTry = determineMinimumTry(totalNumberToCheck, calculatedLog);
        log.info("The calculated minimum try for totalNumberToCheck#{} is minimumTry#{}", totalNumberToCheck, calculatedMinimumTry);
        resultToReturn.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, calculatedMinimumTry);
        return resultToReturn;
    }

    private int determineMinimumTry(int initialTotalNumberToCheck, double calculatedLog) {
        int maximumTry = (int) calculatedLog;

        if (initialTotalNumberToCheck != 1 && (!MathUtils.isEven(initialTotalNumberToCheck))) {
            return Constants.INT_MINIMUM_TRY_FOR_ODD_NUMBER;
        }

        if (MathUtils.isWhole(calculatedLog)) {
            return (maximumTry >= Constants.INT_MAXIMUM_TRY_TO_REACH_FOR_WHOLE_CASE) ? (maximumTry - 1) : maximumTry;
        }

        return calculateMinimumTryFromTotalNumber(initialTotalNumberToCheck, maximumTry);
    }

    private int calculateMinimumTryFromTotalNumber(int initialTotalNumberToCheck, int maximumTry) {
        int calculatedMinimumTry = 1;
        int leftNumberToCheck = initialTotalNumberToCheck;

        while (MathUtils.isEven(leftNumberToCheck) && (calculatedMinimumTry <= maximumTry)) {
            calculatedMinimumTry++;
            leftNumberToCheck = leftNumberToCheck / 2;
        }

        return calculatedMinimumTry;
    }
}
