package oalia.detectTheOddOne.utils;

public class MathUtils {
    public static double calculateLog(int numberToLog, short logBase) {
        return Math.log(numberToLog) / Math.log(logBase);
    }

    public static boolean isWhole(double doubleToCheck) {
        return doubleToCheck % 1 == 0;
    }

    public static boolean isEven(int numberToCheck) {
        return numberToCheck % 2 == 0;
    }
}
