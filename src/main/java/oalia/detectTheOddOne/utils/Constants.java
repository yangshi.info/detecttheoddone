package oalia.detectTheOddOne.utils;

public class Constants {

    public static String STRING_MAXIMUM_TRY_MAP_KEY = "maximumTry";
    public static String STRING_MINIMUM_TRY_MAP_KEY = "minimumTry";
    public static short SHORT_LOG_BASE_2 = 2;
    public static int INT_MINIMUM_TRY_FOR_ODD_NUMBER = 1;
    public static int INT_MAXIMUM_TRY_TO_REACH_FOR_WHOLE_CASE = 2;
}
