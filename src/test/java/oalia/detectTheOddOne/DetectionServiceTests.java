package oalia.detectTheOddOne;

import oalia.detectTheOddOne.service.DetectionService;
import oalia.detectTheOddOne.utils.Constants;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class DetectionServiceTests {

	@Autowired
	DetectionService detectionService;

	@Test
	@DisplayName("Testing calculateOddDetection for 0 as parameter, should throw Exception")
	void testCalculateOddDetectionForZeroAsParameter() {
		Exception exception = assertThrows(Exception.class, () -> {
			detectionService.calculateOddDetection(0);
		});

		String expectedMessage = "The number totalNumberToCheck#0 isn't allowed";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	@DisplayName("Testing calculateOddDetection for 1 as parameter, should be ok")
	void testCalculateOddDetectionForOne() throws Exception {
		Map<String, Integer> actualResults = detectionService.calculateOddDetection(1);
		Map<String, Integer> expectedResults = new HashMap<>();
		expectedResults.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, 0);
		expectedResults.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, 0);
		assertTrue(actualResults.equals(expectedResults));
	}

	@Test
	@DisplayName("Testing calculateOddDetection for 2 as parameter, should be ok")
	void testCalculateOddDetectionForTwo() throws Exception {
		Map<String, Integer> actualResults = detectionService.calculateOddDetection(2);
		Map<String, Integer> expectedResults = new HashMap<>();
		expectedResults.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, 1);
		expectedResults.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, 1);
		assertTrue(actualResults.equals(expectedResults));
	}

	@Test
	@DisplayName("Testing calculateOddDetection for 4 as parameter, should be ok")
	void testCalculateOddDetectionForFour() throws Exception {
		Map<String, Integer> actualResults = detectionService.calculateOddDetection(4);
		Map<String, Integer> expectedResults = new HashMap<>();
		expectedResults.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, 2);
		expectedResults.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, 1);
		assertTrue(actualResults.equals(expectedResults));
	}

	@Test
	@DisplayName("Testing calculateOddDetection for an odd number like 15, should be ok")
	void testCalculateOddDetectionForOddNumber() throws Exception {
		Map<String, Integer> actualResults = detectionService.calculateOddDetection(15);
		Map<String, Integer> expectedResults = new HashMap<>();
		expectedResults.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, 3);
		expectedResults.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, 1);
		assertTrue(actualResults.equals(expectedResults));
	}

	@Test
	@DisplayName("Testing calculateOddDetection for an even number like 1002 which doesn't has a whole log 2 base, should be ok")
	void testCalculateOddDetectionForEvenNumberButNotWhole() throws Exception {
		Map<String, Integer> actualResults = detectionService.calculateOddDetection(1002);
		Map<String, Integer> expectedResults = new HashMap<>();
		expectedResults.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, 9);
		expectedResults.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, 2);
		assertTrue(actualResults.equals(expectedResults));
	}

	@Test
	@DisplayName("Testing calculateOddDetection for an even number like 65536 which has a whole log 2 base, should be ok")
	void testCalculateOddDetectionForEvenNumberButWhole() throws Exception {
		Map<String, Integer> actualResults = detectionService.calculateOddDetection(65536);
		Map<String, Integer> expectedResults = new HashMap<>();
		expectedResults.put(Constants.STRING_MAXIMUM_TRY_MAP_KEY, 16);
		expectedResults.put(Constants.STRING_MINIMUM_TRY_MAP_KEY, 15);
		assertTrue(actualResults.equals(expectedResults));
	}

}
