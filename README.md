# Contexte fonctionnel
Dans un lot de balles, on recherche à déterminer la balle intruse qui a un poids légèrement plus lourd que celui des autres balles standard.
Pour cela, nous utilisons une balance à deux plateaux pour retrouver l'intrus à travers les pesées.
Nous cherchons à connaître pour un lot de N balles, les deux informations suivantes :
- Le nombre maximum de tentatives de pesée qu'il faudrait pour N balles pour retrouver l'intrus du lot
- Le nombre minimum de tentatives de pesée à réaliser lorsque nous nous retrouvons dans un cas de figure où la chance pourrait intervenir 
pour faire des pesées en moins par rapport au maximum de tentatives prévues. Dans le cas où la chance n'est pas intervenue, nous nous retrouvons 
tout de même à réaliser le nombre maximum de tentatives prévues.
Cette application suit un algorithme de calcul pour déterminer ces informations pour N balles en entrée.


# Explication de l'algorithme
Attention, dans toute cette partie, les écritures de type 2^X se lit "2 puissance X".
Exemple : 2^4 = 2 puissance 4 = 16

D'autre part, dans les parties de démonstration, les pesées sontreprésentés de cette façon : 1ère pesée > 2ème pesée > 3ème pesée etc...
Exemple : Pour N=64 => 32/32 > 16/16 > 8/8 > 4/4 > 2/2 > 1/1


1. Nombre de tentatives maximum pour retrouver l'intrus :
Pour un nombre N de balle données, le maximum de tentatives de pesée dans ce contexte peut être calculé en rapprochant N à la puissance de 2 minorant
celui-ci.
Imaginons que N = 510, on peut dire que 2^8 = 256 < N = 510 < 2^9 = 512
La puissance de 2 minorant notre nombre de balle N est 2^8 = 256.
Pour déterminer le nombre maximum de tentatives, il ne reste plus qu'à prendre le nombre de l'exposant comme résultat.
Dans notre cas, nous prenons 2^8, donc le nombre de tentatives maximum est de 8.

Attention, lorsque notre N est exactement le résultat d'une puissance de 2, alors nous prenons directement l'exposant de la puissance qui correspond
à N.
Exemple : N = 16. Or 16 = 2^4. Donc notre nombre de tentatives maximum est de 4 vu que 16 est directement le résultat de 2^4.
Démonstration : 8/8 > 4/4 > 2/2 > 1/1 => 4 pesées maximum


2. Nombre minimum de tentatives minimum pour retrouver l'intrus :
Pour un nombre N de balle données, le minimum de tentatives de pesée dans ce contexte comprend plusieurs archétypes de cas :


	a) Si N est un nombre impair
	
Si N est un nombre impair, alors le nombre de tentative minimum est de 1. Puisqu'en séparant N en 2 lots à part égale de balles, on se retrouve
à isoler une balle de côté. Et la chance peut directement avoir lieu dans cette configuration dès le début. Puisqu'il est possible que la balle
isolée soit effectivement l'intrus.
Attention, ce principe ne fonctionne pas pour N = 1 puisque dans cette situation, nous ne faisons pas de pesées tout simplement.

Exemple : N = 257. En séparant 257 balles en deux lots à part égale, ça nous fait 128 balles sur chaque plateau de la balance.
Et nous nous retrouvons avec une balle à part. Si la chance se produit, la balle isolée est l'intrus, et dans ce cas, nous pouvons arrêter
la pesée dès cet instant. D'où 1 est le nombre minimum de pesée possible avec de la chance dans cette situation.
Démonstration : 128/128 + 1 (minimum) > 64/64 > 32/32 > 16/16 > 8/8 > 4/4 > 1/1 > 1/1


	b) Si N est un nombre pair et est le résultat d'une puissance de 2

Si N est un nombre pair et résultat d'une puissance de 2, alors le nombre minimum de tentatives est égale au nombre maximum de tentative - 1.
Puisque en étant une puissance de 2, N ne sera jamais impair lorsque nous scindons les balles par groupe égale de 2 (sauf à la fin de la série 
de pesée quand il ne restera plus qu'une balle de chaque côté).
Du coup, l'occasion d'épargner des pesées se trouve au moment où on sera à 4 balles restantes. Puisque là, on pourrait être dans la configuration
où nous pesons 2 balles sur 4 restantes en mettant une balle de chaque côté du plateau, et avoir une chance d'avoir l'intrus parmi les balles pesées.
Et dans le pire des cas, nous arriverons au même nombre que celui prédit par le maximum de tentatives.
Si N est supérieur ou égale à 4, on passera forcément par cette étape puisque en séparant le lot de balle par 2 groupes, nous refaisons les puissances
de 2.
Attention, cette règle ne marche pas pour N pair strictement inférieur à 4. On reste sur une seule pesée minimum dans ce cas.

Exemple : N = 512. D'après le point 1. le maximum de tentatives 9 puisque 512 = 2^9. Donc d'après la règle, le minimum de tentative serait de 8.
Nous remarquons qu'en séparant les lots à parts égales, nous atteignons 4 balles restantes à la 8ème pesée.
Démonstration : 256/256 > 128/128 > 64/64 > 32/32 > 16/16 > 8/8 > 4/4 > 1/1 (minimum) > 1/1 => CQFD Le minimum se situe à la 8ème pesée au moment
où il ne reste plus que 4 balles à déterminer. Donc le minimum (8) = maximum - 1 (9-1).


	c) Si N est un nombre pair et n'est pas le résultat d'une puissance de 2
	
Si N est un nombre pair quelconque donc pas le résultat d'une puissance de 2, il n'existe pas de règle généraliste pour déterminer le minimum de
tentatives sans passer par un calcul.
Ce calcul consiste à diviser N en 2 parts égales jusqu'à obtenir un lot de balles de nombre impair à diviser. Et la première occurrance de cette situation
détermine le nombre de pesée minimum pour N.

Exemple : N = 510. Nous allons procéder à la séparation suivante pour déterminer le minimum :
- 1ère pesée : 2 lots de 255 balles de chaque côté
- 2ème pesée : 2 lots de 127 balles de chaque côté + 1 balle isolée => C'est là où nous avons le nombre de pesée minimum puisqu'il est possible que
la balle isolée soit l'intrus avec de la chance.
Donc le nombre de tentative minimum est de 2 dans ce cas.
Démonstration : 255/255 > 127/127 + 1 (minimum) > 63/63 + 1 > 31/31 + 1 > 15/15 + 1 > 7/7 + 1 > 3/3 + 1 > 1/1 + 1
CQFD le nombre de tentative minimum se situe au premier passage de lot de balles restants à un nombre impair.


# Outils utilisés
Java 8, Spring Boot, Maven


# Build
Pour compiler les sources et construire le fichier jar :

mvn clean package


# Lancement de l'application
Le jar de l'application est lançable avec la commande suivante :

java -jar detectTheOddOne-Numéro-de-version.jar nombre_à_entrer

Par exemple, pour faire un calcul de nombre de tentive qu'il faut pour trouver l'intrus parmi un jeu de 1300 balles, voici la commande à exécuter :

java -jar detectTheOddOne-0.0.1-SNAPSHOT.jar 1300


# Affichage des résultats
Le résultat de l'exécution est marqué dans les logs générés sous cette forme :

result for parameter#NombreEntré : maximumTry#Tentative_Maximum_Calculé, minimumTry#Tentative_Minimum_Calculé

Par exemple, voici le résultat affiché pour un jeu de 1300 balles en entré :

result for parameter#1300 : maximumTry#10, minimumTry#3

- parameter#1300 : nombre entré en paramètre de l'application, on recherche à calculer le nombre de tentatives pour un jeu de 1300 balles
- maximumTry#10 : nombre de tentative maximum calculé, pour 1300 balles, le nombre maximum de pesée est de 10
- minimumTry#3 : nombre de tentative minimum calculé, pour 1300 balles, le nombre minimum de pesée est de 3

